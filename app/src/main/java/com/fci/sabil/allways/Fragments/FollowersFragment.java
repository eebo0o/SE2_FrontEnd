package com.fci.sabil.allways.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fci.sabil.allways.Adapters.FollowersAdapter;
import com.fci.sabil.allways.Database_Connection.Connection;
import com.fci.sabil.allways.Database_Connection.ConnectionPostListener;
import com.fci.sabil.allways.Items.Follower;
import com.fci.sabil.allways.MainActivity;
import com.fci.sabil.allways.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ahmed on 27-Apr-16.
 */
public class FollowersFragment extends Fragment {

    private int id;
    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_followers, container, false);
        id = MainActivity.getId();


        HashMap<String,String> params = new HashMap<String,String>();
        params.put("UserID",String.valueOf(id));
        Connection conn = new Connection(params, new ConnectionPostListener() {
            @Override
            public void doAction(String result) {
                try {

                   JSONArray jsonArray = new JSONArray (result);

                    ArrayList<Follower> followers = new ArrayList<Follower>();
                    followers = Follower.fromJson(jsonArray);

                    FollowersAdapter adapter = new FollowersAdapter(getActivity(), followers);
                    ListView listView = (ListView) view.findViewById(R.id.followers);
                    listView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        conn.execute("http://allways-sabil.rhcloud.com/FCISquare/rest/getFollowers");

        return view;
    }
}
